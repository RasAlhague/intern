﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.ConsoleTest
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(typeof(CompilationUnit).Name);

            CompilationUnit compilationUnit = new CompilationUnit();
            compilationUnit.Name = "Test";
            compilationUnit.Namespaces.Add(compilationUnit.CreateChild<NamespaceDeclaration>());
            compilationUnit.Namespaces.Add(compilationUnit.CreateChild<NamespaceDeclaration>());

            foreach (var item in compilationUnit.Namespaces)
            {
                Console.WriteLine("t");
            }

            Console.ReadLine();
        }
    }
}
