﻿using System.Collections.Generic;

namespace SyntaxTreeBuilder
{
    public interface ITopLevelObject : IChildCreator
    {
        List<NamespaceDeclaration> Namespaces { get; set; }
        List<TypeDeclaration> DeclaredTypes { get; set; }
    }
}