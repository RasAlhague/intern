﻿using SyntaxTreeBuilder.Exceptions;
using System;
using System.Collections.Generic;

namespace SyntaxTreeBuilder
{
    public class NamespaceDeclaration : CodeObject, ITopLevelObject, IIdentifiable
    {
        public string Identifier { get; set; }
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is ITopLevelObject)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
        public List<NamespaceDeclaration> Namespaces { get; set; }
        public List<TypeDeclaration> DeclaredTypes { get; set; }
        public List<string> ParentNamespaces { get; set; }

        public NamespaceDeclaration() : this(null)
        {

        }

        public NamespaceDeclaration(CodeObject parent) : this(parent, string.Empty)
        {
        }

        public NamespaceDeclaration(CodeObject parent, string codeSnippet) : base(parent, codeSnippet)
        {
            Namespaces = new List<NamespaceDeclaration>();
            DeclaredTypes = new List<TypeDeclaration>();
            ParentNamespaces = new List<string>();
        }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public T CreateChild<T>() where T : CodeObject
        {
            return (T)Activator.CreateInstance(typeof(T), this);
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}