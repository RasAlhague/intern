﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public class CompilationUnit : CodeObject, IContainsAttribute, ITopLevelObject
    {
        public string Name { get; set; }
        public List<UsingDirective> Usings { get; private set; }
        public List<NamespaceDeclaration> Namespaces { get; set; }
        public List<TypeDeclaration> DeclaredTypes { get; set; }
        public AttributeList Attributes { get; set; }
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                _parent = value;
            }
        }

        public CompilationUnit() : this(null)
        {
        }

        public CompilationUnit(CodeObject parent) : this(parent, string.Empty)
        {
        }

        public CompilationUnit(CodeObject parent, string codeSnippet) : base(parent, codeSnippet)
        {
            Usings = new List<UsingDirective>();
            Namespaces = new List<NamespaceDeclaration>();
            DeclaredTypes = new List<TypeDeclaration>();
            Attributes = new AttributeList();
            RegexMatchList.Add(typeof(UsingNamespaceDirective).Name, "using[\\s]+[.a-zA-Z0-9]+;");
            RegexMatchList.Add(typeof(UsingAliasDirective).Name, "using[\\s]+[a-zA-Z0-9]+[\\s]+=[\\s]+[a-zA-Z0-9.]+;");
        }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public T CreateChild<T>() where T : CodeObject
        {
            return (T)Activator.CreateInstance(typeof(T), this);
        }

        public override void ExtractCode()
        {
            
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
