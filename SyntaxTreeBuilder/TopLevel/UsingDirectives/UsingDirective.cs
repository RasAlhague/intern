﻿using SyntaxTreeBuilder.Exceptions;

namespace SyntaxTreeBuilder
{
    public abstract class UsingDirective : CodeObject
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is ITopLevelObject)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
    }
}