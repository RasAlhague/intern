﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public class UsingAliasDirective : UsingDirective, IIdentifiable
    {
        public string Identifier { get; set; }
        public string NamespaceOrTypeName { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
