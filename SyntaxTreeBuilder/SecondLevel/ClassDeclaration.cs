﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public class ClassDeclaration : TypeDeclaration, IIsPartial
    {
        public ClassModifier ClassModifier { get; set; }
        public bool IsPartial { get; set; }
        public List<MemberDeclaration> Members { get; set; }
        public List<TypeDeclaration> NestedTypes { get; set; }
        public string BaseClass { get; set; }
        public List<string> ImplementedInterfaces { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
