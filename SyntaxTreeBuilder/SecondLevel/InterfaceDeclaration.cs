﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public class InterfaceDeclaration : TypeDeclaration
    {
        public bool IsPartial { get; set; }
        public List<InterfaceMemberDeclaration> Members { get; set; }
        public List<string> BaseInterfaces { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
