﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.SecondLevel
{
    public class EnumDeclaration : TypeDeclaration
    {
        public List<EnumMemberDeclaration> EnumMembers { get; private set; }
        public string EnumBase { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
