﻿using SyntaxTreeBuilder.Exceptions;

namespace SyntaxTreeBuilder
{
    public abstract class TypeDeclaration : CodeObject, IContainsAttribute, IIdentifiable
    {
        public AccessModifier AccessModifier { get; set; }
        public AttributeList Attributes { get; set; }
        public string Identifier { get; set; }
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is TypeDeclaration || value is ITopLevelObject)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
    }
}