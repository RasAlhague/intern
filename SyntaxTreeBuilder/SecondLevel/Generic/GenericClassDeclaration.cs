﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.SecondLevel.Generic
{
    public class GenericClassDeclaration : ClassDeclaration, IGenericObject
    {
        public TypeParameter GenericTypes { get; set; }
        public TypeParameterConstraints GenericConstraints { get; set; }
    }
}
