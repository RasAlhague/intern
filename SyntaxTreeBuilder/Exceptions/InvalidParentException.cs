﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.Exceptions
{
    [Serializable]
    public class InvalidParentException : Exception
    {
        public InvalidParentException() { }
        public InvalidParentException(string message) : base(message) { }
        public InvalidParentException(string message, Exception inner) : base(message, inner) { }
        protected InvalidParentException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
