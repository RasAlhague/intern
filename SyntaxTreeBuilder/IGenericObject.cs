﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IGenericObject
    {
        TypeParameter GenericTypes { get; set; }
        TypeParameterConstraints GenericConstraints { get; set; }
    }
}
