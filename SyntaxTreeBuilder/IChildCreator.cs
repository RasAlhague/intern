﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IChildCreator
    {
        T CreateChild<T>() where T : CodeObject;
    }
}
