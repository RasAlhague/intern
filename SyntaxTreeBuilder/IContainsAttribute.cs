﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IContainsAttribute
    {
        AttributeList Attributes { get; set; }
    }
}
