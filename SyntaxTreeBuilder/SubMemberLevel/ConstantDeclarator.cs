﻿using SyntaxTreeBuilder.Exceptions;

namespace SyntaxTreeBuilder
{
    public class ConstantDeclarator : CodeObject, IIdentifiable
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is ConstantDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
        public string Identifier { get; set; }
        public string ConstantExpression { get; set; }


        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}