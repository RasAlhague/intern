﻿using SyntaxTreeBuilder.Exceptions;
using SyntaxTreeBuilder.MemberLevel.InterfaceMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.SubMemberLevel
{
    public class InterfaceAccessorDeclaration : CodeObject
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is InterfacePropertyDeclaration || value is InterfaceIndexerDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
        public bool HasGet { get; set; }
        public bool HasSet { get; set; }
        public AttributeList GetAttributes { get; set; }
        public AttributeList SetAttributes { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
