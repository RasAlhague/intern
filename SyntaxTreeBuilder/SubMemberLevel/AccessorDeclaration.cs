﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public class AccessorDeclaration : CodeObject
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is PropertyDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidProgramException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
        public AccessModifier GetModifier { get; set; }
        public AccessModifier SetModifier { get; set; }
        public CodeBlock GetBlock { get; set; }
        public CodeBlock SetBlock { get; set; }
        public AttributeList GetAttributes { get; set; }
        public AttributeList SetAttributes { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
