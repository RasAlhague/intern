﻿using SyntaxTreeBuilder.Exceptions;

namespace SyntaxTreeBuilder
{
    public class EventAccessorDeclaration : CodeObject
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is EventDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
        public CodeBlock AddBlock { get; set; }
        public CodeBlock RemoveBlock { get; set; }
        public AttributeList AddAttributes { get; set; }
        public AttributeList RemoveAttributes { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}