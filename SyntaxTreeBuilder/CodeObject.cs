﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public abstract class CodeObject
    {
        protected string _codeSnippet;
        protected CodeObject _parent;

        public Dictionary<string, string> RegexMatchList { get; protected set; }
        public string CodeTemplate { get; set; }
        public abstract CodeObject Parent { get; protected set; }

        protected CodeObject() : this(null, string.Empty)
        {

        }

        protected CodeObject(CodeObject parent) : this(parent, string.Empty)
        {

        }

        protected CodeObject(CodeObject parent, string codeSnippet)
        {
            RegexMatchList = new Dictionary<string, string>();
            _codeSnippet = codeSnippet;
            _parent = parent;
        }

        public abstract string BuildCode();
        public abstract void ExtractCode();
        public abstract void ExtractCode(string codeSnippet);
    }
}
