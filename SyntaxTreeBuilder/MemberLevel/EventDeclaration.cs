﻿namespace SyntaxTreeBuilder
{
    public class EventDeclaration : MemberDeclaration, IIdentifiable
    {
        public EventModifier EventModifier { get; set; }
        public string Type { get; set; }
        public string Identifier { get; set; }
        public EventAccessorDeclaration EventAccessor { get;set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}