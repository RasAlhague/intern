﻿namespace SyntaxTreeBuilder
{
    public class IndexerDeclaration : MemberDeclaration, IIndexer
    {
        public string Type { get; set; }
        public FormalParameterList FormalParameters { get; set; }
        public IndexerModifier IndexerModifier { get; set; }
        public string InterfaceType { get; set; }
        public AccessorDeclaration Accessor { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}