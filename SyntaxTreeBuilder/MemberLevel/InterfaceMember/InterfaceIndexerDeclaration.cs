﻿using SyntaxTreeBuilder.SubMemberLevel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.MemberLevel.InterfaceMember
{
    public class InterfaceIndexerDeclaration : InterfaceMemberDeclaration, IIndexer
    {
        public string Type { get; set; }
        public FormalParameterList FormalParameters { get; set; }
        public InterfaceAccessorDeclaration InterfaceAccessor { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
