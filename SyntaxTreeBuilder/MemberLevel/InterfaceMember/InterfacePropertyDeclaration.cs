﻿using SyntaxTreeBuilder.SubMemberLevel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.MemberLevel.InterfaceMember
{
    public class InterfacePropertyDeclaration : InterfaceMemberDeclaration, IProperty
    {
        public string Type { get; set; }
        public string Identifier { get; set; }
        public InterfaceAccessorDeclaration InterfaceAccessor { get; set; }

        public override string BuildCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new NotImplementedException();
        }
    }
}
