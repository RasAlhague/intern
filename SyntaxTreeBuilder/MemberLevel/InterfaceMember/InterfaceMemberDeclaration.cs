﻿using SyntaxTreeBuilder.Exceptions;

namespace SyntaxTreeBuilder
{
    public abstract class InterfaceMemberDeclaration : CodeObject, IContainsAttribute
    {
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is InterfaceDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Type of value is not of type \"" + typeof(InterfaceDeclaration) + "\". value is of type \"" + value.GetType() + "\"" );
                }
            }
        }
        public AttributeList Attributes { get; set; }
    }
}