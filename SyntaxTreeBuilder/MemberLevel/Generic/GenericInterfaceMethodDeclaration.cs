﻿using SyntaxTreeBuilder.MemberLevel.InterfaceMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder.MemberLevel.Generic
{
    public class GenericInterfaceMethodDeclaration : InterfaceMethodDeclaration, IGenericObject
    {
        public TypeParameter GenericTypes { get; set; }
        public TypeParameterConstraints GenericConstraints { get; set; }
    }
}
