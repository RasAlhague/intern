﻿using SyntaxTreeBuilder.Exceptions;
using System.Collections.Generic;

namespace SyntaxTreeBuilder
{
    public class ConstantDeclaration : MemberDeclaration
    {
        public string Type { get; set; }
        public List<ConstantDeclarator> Declarators { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}