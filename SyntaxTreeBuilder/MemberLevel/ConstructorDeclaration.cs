﻿namespace SyntaxTreeBuilder
{
    public class ConstructorDeclaration : MemberDeclaration, IIdentifiable
    {
        public string Identifier { get; set; }
        public FormalParameterList FormalParameters { get; set; }
        public string Initializer { get; set; }
        public CodeBlock Code { get; set; }
        public bool IsStatic { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}