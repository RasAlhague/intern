﻿using System.Collections.Generic;

namespace SyntaxTreeBuilder
{
    public class FieldDeclaration : MemberDeclaration
    {
        public List<VariableDeclarator> VariableDeclarators { get; set; }
        public FieldModifier FieldModifier { get; set; }
        public string Type { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}