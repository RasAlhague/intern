﻿namespace SyntaxTreeBuilder
{
    public class DestructorDeclaration : MemberDeclaration, IIdentifiable
    {
        public string Identifier { get; set; }
        public CodeBlock Code { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}