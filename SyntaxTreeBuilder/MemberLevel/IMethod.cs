﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IMethod : IIdentifiable
    {
        string ReturnType { get; set; }
        FormalParameterList FormalParameters { get; set; }
    }
}
