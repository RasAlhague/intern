﻿using SyntaxTreeBuilder.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public abstract class MemberDeclaration : CodeObject, IContainsAttribute
    {
        public virtual AccessModifier AccessModifier { get; set; }
        public AttributeList Attributes { get; set; }
        public override CodeObject Parent
        {
            get
            {
                return _parent;
            }
            protected set
            {
                if(value is ClassDeclaration || value is StructDeclaration)
                {
                    _parent = value;
                }
                else
                {
                    throw new InvalidParentException("Invalid value! value is of type \"" + value.GetType() + "\"");
                }
            }
        }
    }
}
