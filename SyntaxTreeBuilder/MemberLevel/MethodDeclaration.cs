﻿using SyntaxTreeBuilder.MemberLevel;

namespace SyntaxTreeBuilder
{
    public class MethodDeclaration : MemberDeclaration, IMethod, IIsPartial
    {
        public string ReturnType { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
        public string Identifier { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
        public FormalParameterList FormalParameters { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
        public MethodModifier MethodModifier { get; set; }
        public bool IsAsync { get; set; }
        public bool IsPartial { get; set; }
        public CodeObject Block { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}