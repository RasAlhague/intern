﻿namespace SyntaxTreeBuilder
{
    public class PropertyDeclaration : MemberDeclaration, IIdentifiable
    {
        public string Identifier { get; set; }
        public PropertyModifier PropertyModifier { get; set; }
        public string ExpressionBodiedTemplate { get; set; }
        public AccessorDeclaration AccessorDeclaration { get; set; }
        public string VariableInitializer { get; set; }

        public override string BuildCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode()
        {
            throw new System.NotImplementedException();
        }

        public override void ExtractCode(string codeSnippet)
        {
            throw new System.NotImplementedException();
        }
    }
}