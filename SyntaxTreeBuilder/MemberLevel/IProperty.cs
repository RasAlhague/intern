﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IProperty : IIdentifiable
    {
        string Type { get; set; }
    }
}
