﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyntaxTreeBuilder
{
    public interface IIndexer
    {
        string Type { get; set; }
        FormalParameterList FormalParameters { get; set; }
    }
}
