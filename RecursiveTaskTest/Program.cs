﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RecursiveTaskTest
{
	public static class Program
	{
		public static Mutex mutex = new Mutex();
		public static int i;

		static void Main(string[] args)
		{
			Rekursion();

			Console.ReadLine();
		}

		public static async void Rekursion()
		{
			Thread.Sleep(1);


			mutex.WaitOne();
			i++;
			Console.WriteLine(Thread.CurrentThread.ManagedThreadId + " ; " + i);
			mutex.ReleaseMutex();

			await Task.Run(() => Rekursion());

			mutex.WaitOne();
			i--;
			mutex.ReleaseMutex();
		}
	}
}
