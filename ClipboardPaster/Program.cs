using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
 
namespace ClipboardPaster
{
    /// <summary>
    /// Programm, which copies a text from the clipboard and writes it to a file, for further processing
    /// 
    /// Parameter: -name= | -path= | -exit=
    /// -name: name of the file which should be created.
    /// -path: path were the file should be created at.
    /// -exit: close the programm after succesfully writing.
    /// </summary>
    public static class Program
    {
       
        /// <summary>
        /// Einstiegspunkt. Ließt die argumente aus und started die verabeitung.
        /// </summary>
        /// <param name="args">Comandline Argumente</param>
        [STAThread]
        public static void Main(string[] args)
        {
            Config config = null;
            Console.WriteLine("Clipboard Pasting: V.0.0.1");
            try
            {
                config = new Config(args);
                Run(config);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
            if(config != null && config.CloseOnFinish)
            {
                Environment.Exit(0);
            }
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey();
        }
        /// <summary>
        /// Verarbeitet die Configuration. Schreibt den Inhalt des Clipboards wenn gegeben an die angegebene position.
        /// </summary>
        /// <param name="config">Configuration für das schreiben der datei.</param>
        public static void Run(Config config)
        {
            if (Clipboard.ContainsText())
            {
                string clippedText = Clipboard.GetText();
                string completePath = Path.Combine(config.PastePath, config.PasteName);
                using (var sw = new StreamWriter(new FileStream(completePath, FileMode.Create)))
                {
                    sw.Write(clippedText);
                }
                Console.WriteLine("-> File created and written...");
            }
            else
            {
                // Cause of why not
                Console.WriteLine($"{GetStarcraftQuote(config.QuotesFilePath)} (empty clipboard)");
            }
        }
        /// <summary>
        /// Hatte langeweile also gibt es jetzt noch Starcraft Quotes xD
        /// </summary>
        /// <returns>Einen von 5 vorher definierten quotes, UUUUU die kann ich aus ner datei laden XD</returns>
        public static string GetStarcraftQuote(string path)
        {
            Dictionary<int, string> sentences = new Dictionary<int, string>();
            string[] quotes = File.ReadAllLines(path);
            Random r = new Random();
            for (int i = 0; i < quotes.Length; i++)
            {
                sentences.Add(i, quotes[i]);
            }
            return sentences[r.Next(0, quotes.Length)];
        }
    }
}