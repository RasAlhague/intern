using System;
using System.Linq;
 
namespace ClipboardPaster
{
    /// <summary>
    /// Klasse zum Speichern der Konfigurationsdatei.
    /// </summary>
    public class Config
    {
        /// <summary>
        /// Path were the content should be pasted to.
        /// </summary>
        public string PastePath { get; }
        /// <summary>
        /// Name of the file which is created.
        /// </summary>
        public string PasteName { get; }
        /// <summary>
        /// 
        /// </summary>
        public string QuotesFilePath { get; }
        /// <summary>
        /// Programm schließt nach erfolgreichen schreiben.
        /// </summary>
        public bool CloseOnFinish { get; }
        /// <summary>
        /// Erzeugt aus den Commandline Argumenten die Configurationparameter.
        /// </summary>
        /// <param name="args">Commandline Argumente.</param>
        public Config(string[] args)
        {
            if(args.Length < 3)
            {
                throw new ArgumentException("Not enough minerals");
            }
            PastePath = args.First(x => x.Contains("-path=")).Replace("-path=", "");
            PasteName = args.First(x => x.Contains("-name=")).Replace("-name=", "");
            QuotesFilePath = args.First(x => x.Contains("-quotes=")).Replace("-quotes=", "");
            CloseOnFinish = args.FirstOrDefault(x => x.Contains("-exit")) != null;
        }
    }
}